package xyz.ismayil.sfgpetclinic.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import xyz.ismayil.sfgpetclinic.model.Owner;
import xyz.ismayil.sfgpetclinic.model.Vet;
import xyz.ismayil.sfgpetclinic.services.OwnerService;
import xyz.ismayil.sfgpetclinic.services.VetService;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;


    public DataLoader(OwnerService ownerService, VetService vetService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
    }

    @Override
    public void run(String... args) throws Exception {
        Owner owner1 = new Owner();
        owner1.setFirstName("Ismayil");
        owner1.setLastname("Ismayilov");
        ownerService.save(owner1);

        Owner owner2 = new Owner();


        owner2.setFirstName("Vafa");
        owner2.setLastname("Fatullayeva");
        ownerService.save(owner2);

        System.out.println("Loaded Owners....");

        Vet vet1 = new Vet();

        vet1.setFirstName("Ayi");
        vet1.setLastname("Wengulum");
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("puppy");
        vet2.setLastname("asd");
        vetService.save(vet2);
        System.out.println("Loaded Vets..");

    }
}
