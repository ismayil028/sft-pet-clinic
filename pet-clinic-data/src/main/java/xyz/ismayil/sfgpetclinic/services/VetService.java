package xyz.ismayil.sfgpetclinic.services;

import xyz.ismayil.sfgpetclinic.model.Vet;

public interface VetService extends CrudService<Vet, Long> {

}
