package xyz.ismayil.sfgpetclinic;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import xyz.ismayil.sfgpetclinic.model.Pet;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Pet.class)
public class SfgPetClinicApplicationTests {

    @Test
    public void contextLoads() {
    }

}
